import java.io.*;
import java.util.*;

class Board {

  /**
   * Definición de la matriz que representa el tablero del juego
   *
   * Valores de la matriz y su significado
   *
   * 0 - Casilla vacía
   * 1 - Jugador X
   * 2 - Jugador O
   */
  static int[][] board = new int[3][3];

  /**
   * Inicializa el tablero board, colocando un 0 en cada una de sus posiciones
   *
   * @return void
   */
  public static void initializeBoard() {
    for (int i = 0; i < board.length; i++) {
      for (int j = 0; j < board[0].length; j++) {
        board[i][j] = 0;
      }
    }
  }

  /**
   * Imprime por la salida estándar el estado actual del tablero board.
   * 0 - Casilla vacía
   * 1 - Jugador X
   * 2 - Jugador O
   *
   * @return void
   */
  public static void printBoard() {
    for (int i = 0; i < board.length; i++) {
      for (int j = 0; j < board[0].length; j++) {

        if (board[i][j] == 0) System.out.print("  | ");
        if (board[i][j] == 1) System.out.print("X | ");
        if (board[i][j] == 2) System.out.print("O | ");

      }
        System.out.println();
        System.out.println("___________");

    }
  }

  /**
   * Coloca en la casilla indicada por la coordanada x e y la pieza indicada
   * en función del player elegido. Si el player es 1, colocará un 1 en la
   * matriz board, si el player es 2 colocará un 2.
   *
   * @param player  El jugador que realiza el movimiento (1 o 2)
   * @param tirada La coordenada x, y del tablero en la que se realiza el
   *               movimiento.
   *
   * @return  true si la pieza pudo colocarse en la coordenda indicada,
   *          false en caso contrario.
   */
  public static Boolean play(int player, Coordenate tirada) {

    if (player == 1 || player == 2) {
      board[tirada.x-1][tirada.y-1] = player;
      return true;
    }
    return false;

  }

  /**
   * Coloca en la casilla indicada por la coordanada x e y la pieza indicada
   * en función del player elegido. Si el player es 1, colocará un 1 en la
   * matriz board, si el player es 2 colocará un 2.
   *
   * @param player  El jugador que realiza el movimiento (1 o 2)
   * @param x La fila x del tablero en la que se realiza el movimiento
   * @param y La columna y del tablero en la que se realiza el movimiento
   *
   * @return  true si la pieza pudo colocarse en la coordenda indicada,
   *          false en caso contrario.
   */
  public static Boolean play(int player, int x, int y) {

      if (player == 1 || player == 2) {
        board[x-1][y-1] = player;
        return true;
      }
      return false;
  }


}
