import java.io.*;
import java.util.*;

/**
 * Clase Coordenate que simula una coordenada x, y en la matriz que
 * representa nuestro tablero board.
 */
class Coordenate {
  /**
  * Atributos de la clase Coordenate
  */
  int x;
  int y;

  /**
  * Constructor de la clase Coordenate, crea un objeto vacío
  */
  public Coordenate() { }

  /**
  * Constructor de la clase Coordenate, crea un objeto con los valores pasados
  * como parámetro.
  */
  public Coordenate(int x, int y) {
    this.x = x;
    this.y = y;
  }
}
