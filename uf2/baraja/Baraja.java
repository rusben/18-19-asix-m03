import java.io.*;
import java.util.*;

/*

Vamos a hacer una baraja de cartas españolas orientado a objetos.

Una carta tiene un número entre 1 y 12 (el 8 y el 9 no los incluimos) y un palo (espadas, bastos, oros y copas)

La baraja estará compuesta por un conjunto de cartas, 40 exactamente.

Las operaciones que podrá realizar la baraja son:

    barajar: cambia de posición todas las cartas aleatoriamente
    siguienteCarta: devuelve la siguiente carta que está en la baraja, cuando no haya más o se haya llegado al final, se indica al usuario que no hay más cartas.
    cartasDisponibles: indica el número de cartas que aún puede repartir
    darCartas: dado un número de cartas que nos pidan, le devolveremos ese número de cartas (piensa que puedes devolver). En caso de que haya menos cartas que las pedidas, no devolveremos nada pero debemos indicárselo al usuario.
    cartasMonton: mostramos aquellas cartas que ya han salido, si no ha salido ninguna indicárselo al usuario
    mostrarBaraja: muestra todas las cartas hasta el final. Es decir, si se saca una carta y luego se llama al método, este no mostrara esa primera carta.


*/

class Baraja {

    private Carta[] baraja = new Carta[40];

    public Baraja() {

      String palos[] = {"oros", "copas", "espadas", "bastos"};

      int c = 0;
      for (int i = 0; i < 4; i++) {
        for (int j = 1; j <= 12; j++) {
          if (j != 8 && j != 9) {
            baraja[c] = new Carta(j, palos[i]);
            c++;
          }
        }
      }
    }

    // barajar: cambia de posición todas las cartas aleatoriamente
    void barajar() {

      int min = 0;
      int max = this.baraja.length;
      int intercambios = 2500;

      // create instance of Random class
      Random randomNum = new Random();

      for (int i = 0; i < intercambios; i++) {

        int a = min + randomNum.nextInt(max);
        int b = min + randomNum.nextInt(max);

        intercambiar(a, b);

      }

    }

    // mostrarBaraja: muestra todas las cartas hasta el final. Es decir, si se saca una carta y luego se llama al método, este no mostrara esa primera carta.
    void mostrarBaraja() {
      for (int i = 0; i < baraja.length; i++) {
        System.out.println(baraja[i]);
      }
    }

    // siguienteCarta: devuelve la siguiente carta que está en la baraja, cuando no haya más o se haya llegado al final, se indica al usuario que no hay más cartas
    Carta siguienteCarta() {

      for (int i = 0; i < baraja.length; i++) {
        if(!baraja[i].isMostrada()) {
          baraja[i].setMostrada(true);
          return baraja[i];
        }
      }

      return null;

    }

    // cartasDisponibles: indica el número de cartas que aún puede repartir
    int cartasDisponibles() {

      int noMostradas = 0;

      for (int i = 0; i < baraja.length; i++) {
        if(!baraja[i].isMostrada()) {
          noMostradas++;
        }
      }

      return noMostradas;
    }

    // darCartas: dado un número de cartas que nos pidan,
    // le devolveremos ese número de cartas (piensa que puedes devolver).
    // En caso de que haya menos cartas que las pedidas,
    // no devolveremos nada pero debemos indicárselo al usuario.

    Carta[] darCartas(int numeroCartas) {

      Carta[] cartas = null;

      if (numeroCartas <= cartasDisponibles()) {
          cartas = new Carta[numeroCartas];

          for (int i = 0; i < numeroCartas; i++) {
            cartas[i] = siguienteCarta();
          }

      } else {
        // ERROR
      }

      return cartas;

    }

    // cartasMonton: mostramos aquellas cartas que ya han salido,
    // si no ha salido ninguna indicárselo al usuario

    Carta[] cartasMonton() {

      int nMostradas =  baraja.length - cartasDisponibles();
      Carta[] cartas = new Carta[nMostradas];

        int c = 0;

        for (int i = 0; i < baraja.length; i++) {
          if(baraja[i].isMostrada()) {
            cartas[c] = baraja[i];
            c++;
          }
        }

        return cartas;

    }

    private void intercambiar(int a, int b) {

      Carta tmp = baraja[a];
      baraja[a] = baraja[b];
      baraja[b] = tmp;

    }

}
