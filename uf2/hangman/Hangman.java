import java.io.*;
import java.util.*;

public class Hangman {

  int errors;
  String secret;
  char[] found;

  void initializeHangman() {
    errors = 0;
    secret = "palabra";
    initializeFound();
  }

  void increaseErrors() {
    errors++;
  }

  void initializeFound() {
    found = new char[secret.length()];
    for (int i = 0; i < found.length; i++) {
        found[i] = '_';
    }
  }

  char askCharacter() {
    System.out.println("Introduce un caracter: ");
    Scanner in = new Scanner(System.in);
    char c = in.next(".").charAt(0);

    return c;
  }

  Boolean isC(char c) {
    for (int i=0; i < secret.length(); i++ ) {
      if (secret.charAt(i) == c) return true;
    }

    return false;
  }

  Boolean isAlive() {
    if (errors <= 5) return true;
    else return false;
  }

  Boolean isCompleted() {
    for (int i=0; i < found.length; i++ ) {
      if (found[i] == '_') return false;
    }
    return true;
  }

  void updateSecretWord(char daniel) {

    for (int i=0; i < secret.length(); i++ ) {
      if (secret.charAt(i) == daniel) {
        found[i] = daniel;
      }
    }
  }

  void printAme() {
    for (int i=0; i < secret.length(); i++ ) {
       System.out.print(found[i]+" ");
    }
    System.out.println();
    System.out.println(errors);
  }

  public static void main(String[] args) {

    Hangman hangman = new Hangman();
    hangman.initializeHangman();

    while (hangman.isAlive() && !hangman.isCompleted()) {

        char c = hangman.askCharacter();

        if (hangman.isC(c)) {
            hangman.updateSecretWord(c);

        } else {
            hangman.increaseErrors();
        }

        hangman.printAme();
    }
  }
}
