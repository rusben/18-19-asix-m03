package net.xeill.elpuig.view;

import net.xeill.elpuig.model.*;
import net.xeill.elpuig.controller.*;

import net.xeill.elpuig.view.widget.*;


public class PantallaCreatePlayer {
    public static void mostrar(){

        Missatge.mostrarTitol("PETANCA :: Players :: Create");

        while(true){
            String name = LectorTeclat.llegirString("Name:");
            if("".equals(name)){
                return;
            }

            int age = LectorTeclat.llegirInt("Age:");
            if("".equals(age)){
                return;
            }

            while(true) {
              //  int idEquip = LectorTeclat.llegirInt("Equip:");

              Team teams[] = TeamController.getTeams();

                if (teams.length == 0) {
                    Missatge.mostrarError("Encara no existeix cap equip. Crea l'equip primer.");
                } else {
                  TeamWidget.list(teams);

                  int teamId = LectorTeclat.llegirInt("Id equip:");
                  Team team = TeamController.getTeamById(teamId);

                    if (team == null) {
                        Missatge.mostrarError("No s'ha trobat l'equip amb id " + teamId);
                    } else {
                        TeamWidget.select(team);

                        Player p = PlayerController.createPlayer(name, age, true, teamId);

                        PlayerWidget.select(p);
                        Missatge.mostrarOk("Player creat correctament");
                        LectorTeclat.llegirContinuar();

                        return;
                    }
                }
            }

        }
    }
}
