package net.xeill.elpuig.view;



import net.xeill.elpuig.view.widget.LectorTeclat;
import net.xeill.elpuig.view.widget.Missatge;
import net.xeill.elpuig.view.PantallaCreatePlayer;

public class PantallaMenuPlayers {
    public static void mostrar(){
        while(true) {
            Missatge.mostrarTitol("PETANCA :: Players");
            System.out.println("a) Create player");
            System.out.println("b) List players");
            System.out.println("c) Update player");
            System.out.println("d) Delete player");
            System.out.println("*) Back");

            String opcio = LectorTeclat.llegirOpcio();

            switch (opcio) {
                case "a":
                    PantallaCreatePlayer.mostrar();
                    break;
                case "b":
                    //PantallaLlistaCorredors.mostrar();
                    break;
                case "c":
                    //PantallaModificarCorredor.mostrar();
                    break;
                case "d":
                    //PantallaEsborrarCorredor.mostrar();
                    break;
                default:
                    return;
            }
        }
    }
}
