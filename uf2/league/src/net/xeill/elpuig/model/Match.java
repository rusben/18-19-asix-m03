package net.xeill.elpuig.model;

public class Match {

  private int id;
  private int localId;
  private int visitantId;
  private int localScore;
  private int visitantScore;
  private int journeyId;

  public Match(int id, int localId, int visitantId, int journeyId){
    this.id = id;
    this.localId = localId;
    this.visitantId = visitantId;
    this.localScore = 0;
    this.visitantScore = 0;
    this.journeyId = journeyId;
  }

  public int getId() {
    return this.id;
  }

  public void setId(int value) {
    this.id = value;
  }

  public int getLocalId() {
    return this.localId;
  }

  public void setLocalId(int value) {
    this.localId = value;
  }

  public int getVisitantId() {
    return this.visitantId;
  }

  public void setVisitantId(int value) {
    this.visitantId = value;
  }

  public int getLocalScore() {
    return this.localScore;
  }

  public void setLocalScore(int value) {
    this.localScore = value;
  }

  public int getVisitantScore() {
    return this.visitantScore;
  }

  public void setVisitantScore(int value) {
    this.visitantScore = value;
  }

  public void setJourneyId(int value) {
    this.journeyId = value;
  }

  public int getJourneyId() {
    return this.journeyId;
  }

}
