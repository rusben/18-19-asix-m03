
/*
    Implementa el método Marciano.recibirDaño()
        Recibe una cantidad de daño y disminuye la vida en dicha cantidad. La vida no puede quedar negativa. Si no
        le queda vida el estado debe pasar a "MUERTO".
 */


//import java.util.Random;
//
//class Marciano {
//    int vida;
//    String estado;
//
//    public String toString(){
//        if(vida > 0){
//            return estado + " " + vida;
//        } else {
//            return estado + " " + vida;
//        }
//    }
//}
//
//public class E14 {
//
//    public static void main(String[] args) {
//        Random random = new Random();
//
//        // Incializar el array de marcianos, asignando una vida incial de 100
//	    Marciano[] marcianos = new Marciano[5];
//        for (int i = 0; i < marcianos.length; i++) {
//            marcianos[i] = new Marciano();
//            marcianos[i].vida = 100;
//            marcianos[i].estado = "VIVO";
//        }
//
//        // realizar 10 disparos, a un marciano aleatorio, y con un daño aleatorio
//        for (int i = 0; i < 10; i++) {
//            int marcianoIndex = random.nextInt(marcianos.length);
//            int daño = random.nextInt(150)+50;
//
//            System.out.println("\n----- DISPARO " + i + ": marcianoIndex = " + marcianoIndex + "; daño = " + daño);
//
//            marcianos[marcianoIndex].recibirDaño(daño);
//            printMarcianos(marcianos);
//        }
//    }
//
//    static void printMarcianos(Marciano[] marcianos){
//        for (int i = 0; i < marcianos.length; i++) {
//            System.out.println("marcianos["+i+"].vida = "+marcianos[i]);
//        }
//    }
//}
