/*
    Implementa los métodos siguientes:

     Clock.tick()
        debe aumentar en un segundo el tiempo del reloj

     Clock.reset()
        debe poner a 0 el tiempo del reloj

     Clock.getTime()
        debe retornar el tiempo del reloj con el formato hh:mm:ss
 */

//class Clock {
//    int hours;
//    int minutes;
//    int seconds;
//}
//
//public class E12 {
//
//    public static void main(String[] args) {
//	    Clock clock = new Clock();
//
//
//        System.out.println(clock.getTime()); // 00:00:00
//
//
//
//
//        clock.tick();
//        System.out.println(clock.getTime()); // 00:00:01
//
//
//
//
//	    clock.reset();
//        System.out.println(clock.getTime()); // 00:00:00
//
//
//
//
//        for (int i = 0; i < 60*60; i++) {
//            clock.tick();
//        }
//        System.out.println(clock.getTime()); // 01:00:00
//
//
//
//
//        for (int i = 0; i < 60*60*4+60*2+9; i++) {
//            clock.tick();
//        }
//        System.out.println(clock.getTime()); // 05:02:09
//
//
//
//
//        clock.reset();
//        for (int i = 0; i < 60*60*15+10*60+59; i++) {
//            clock.tick();
//        }
//        System.out.println(clock.getTime()); // 15:10:59
//    }
//}
