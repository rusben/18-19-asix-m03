/*
    Completa la clase LineSeparator:
        Añade los campos que falten

    Completa el método E3.main()
        Llama al método LineSeparator.print() adecuadamente
 */

class LineSeparator {
   int size;
   char charSeparator;

   void print(){
       for (int i = 0; i < size; i++) {
           System.out.print(charSeparator);
       }
       System.out.println();
   }
}


public class E3 {

   public static void main(String[] args) {
       LineSeparator lineSeparator1 = new LineSeparator();
       LineSeparator lineSeparator2 = new LineSeparator();

       lineSeparator1.charSeparator = '-';
       lineSeparator1.size = 20;

       lineSeparator2.charSeparator = '*';
       lineSeparator2.size = 10;

       System.out.println("Debajo de esta linea debe aparecer una linea de 20 guiones");
       lineSeparator1.print();

       System.out.println("Y debajo de esta uno de 10 asteriscos");
       lineSeparator2.print();
   }
}
