/*
    Completa los métodos Termometro.printFahrenheit() y Termometro.printKelvin()
 */

class Termometro {
   float celsius;

   void printCelsius(){
     System.out.println(celsius + "ºC");
   }

   void printFahrenheit(){
     System.out.println((celsius * 9/5) + 32 + "ºF");
   }

   void printKelvin(){
     System.out.println(celsius + 273.15 + "K");
   }
}


public class E4 {

   public static void main(String[] args) {
       Termometro termometro1 = new Termometro();
       Termometro termometro2 = new Termometro();

       termometro1.celsius = 25;
       termometro2.celsius = -25;


       termometro1.printCelsius();
       termometro1.printFahrenheit();
       termometro1.printKelvin();

       termometro2.printCelsius();
       termometro2.printFahrenheit();
       termometro2.printKelvin();
   }
}
