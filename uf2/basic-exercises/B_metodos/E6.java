/*
Implementa los métodos GearBox.gearUp() y GearBox.gearDown()

    gearUp()
        incrementa la 'gear' en 1. Cuando se llega el límite 'numGears', no se debe incrementar

    gearDown()
        decrementa la 'gear' en 1. Cuando se llega a '-1' (marcha atrás) no se debe decrementar

 */

class GearBox {
   int gear;
   int numGears;

   void show(){
       System.out.print("Current gear: ");

       switch (gear){
           case -1:
               System.out.println("R");
               break;
           case 0:
               System.out.println("N");
               break;
           default:
               System.out.println(gear);
       }
   }

   void gearUp() {
     if (gear < numGears) gear++;
   }
   void gearDown() {
     if (gear > -1) gear--;
   }
}

public class E6 {

   public static void main(String[] args) {
	    GearBox gearBox = new GearBox();
      gearBox.numGears = 5;

	    gearBox.gearUp();

	    gearBox.show(); // 1

       gearBox.gearUp();
       gearBox.gearUp();

       gearBox.show(); // 3

       gearBox.gearUp();
       gearBox.gearUp();
       gearBox.gearUp();

       gearBox.show(); // 5

       gearBox.gearDown();
       gearBox.gearDown();
       gearBox.gearDown();
       gearBox.gearDown();
       gearBox.gearDown();

       gearBox.show(); // 0

       gearBox.gearDown();
       gearBox.gearDown();
       gearBox.gearDown();

       gearBox.show(); // -1

   }
}
