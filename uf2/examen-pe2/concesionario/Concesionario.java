class Concesionario {

  private Coche[][] garaje;

  Concesionario() {
    this.garaje = new Coche[10][10];
    initGaraje();
  }

  private void initGaraje() {
    for (int i = 0; i < this.garaje.length; i++) {
      for (int j = 0; j < this.garaje[0].length; j++) {
        this.garaje[i][j] = null;
      }
    }
  }

  public boolean insertarCoche(Coche c) {
    for (int i = 0; i < this.garaje.length; i++) {
      for (int j = 0; j < this.garaje[0].length; j++) {
        if (this.garaje[i][j] == null) {
            this.garaje[i][j] = c;
            return true;
        }
      }
    }
    return false;
  }

  public void imprimirGaraje() {

    for (int i = 0; i < this.garaje.length; i++) {
      for (int j = 0; j < this.garaje[0].length; j++) {
        if (this.garaje[i][j] != null) {
          System.out.println(this.garaje[i][j].getMatricula());
        }
      }
    }

  }

  public Coche buscarCochePorMatricula(String matricula) {
    for (int i = 0; i < this.garaje.length; i++) {
      for (int j = 0; j < this.garaje[0].length; j++) {
        if (this.garaje[i][j] != null) {
          if (matricula.equals(this.garaje[i][j].getMatricula())) {
            return this.garaje[i][j];
          }
        }
      }
    }
    return null;
  }

  public Coche[] buscarCochesPorFila(int fila) {
    return this.garaje[fila];
  }

  public Coche[] buscarCochesPorMarca(String marca) {
    Coche[] coches = new Coche[cuentaMarca(marca)];

    int k = 0;

    for (int i = 0; i < this.garaje.length; i++) {
      for (int j = 0; j < this.garaje[0].length; j++) {
        if (this.garaje[i][j] != null) {
          if (marca.equals(this.garaje[i][j].getMarca().getNombre())) {
            coches[k] = this.garaje[i][j];
            k++;
          }
        }
      }
    }
    return coches;

  }

  public Coche[] buscarCochesPorGarantia(int garantia) {
    Coche[] coches = new Coche[cuentaGarantia(garantia)];
    int k = 0;

    for (int i = 0; i < this.garaje.length; i++) {
      for (int j = 0; j < this.garaje[0].length; j++) {
        if (this.garaje[i][j] != null) {
          if (this.garaje[i][j].getMarca().getGarantia() >= garantia) {
            coches[k] = garaje[i][j];
            k++;
          }
        }
      }
    }
    return coches;
  }

  private int cuentaMarca(String marca) {
    int total = 0;
    for (int i = 0; i < this.garaje.length; i++) {
      for (int j = 0; j < this.garaje[0].length; j++) {
        if (this.garaje[i][j] != null) {
          if (marca.equals(this.garaje[i][j].getMarca().getNombre())) {
            total++;
          }
        }
      }
    }
    return total;
  }

  private int cuentaGarantia(int garantia) {
    int total = 0;
    for (int i = 0; i < this.garaje.length; i++) {
      for (int j = 0; j < this.garaje[0].length; j++) {
        if (this.garaje[i][j] != null) {
          if (this.garaje[i][j].getMarca().getGarantia() >= garantia) {
            total++;
          }
        }
      }
    }
    return total;
  }

  public static void main(String[] args) {

    Marca f = new Marca("Ferrari", 10);
    Marca m = new Marca("Mercedes", 5);
    Marca t = new Marca("Toyota", 4);

    Coche a = new Coche("Testarossa", f, "1234M", 1000, 30000.0f, 10);
    Coche b = new Coche("Vito", m, "3322T", 10000, 5000.0f, 8);
    Coche d = new Coche("Auris", t, "4322G", 20000, 10000.0f, 6);

    Concesionario c = new Concesionario();

    c.insertarCoche(a);
    c.insertarCoche(b);
    c.insertarCoche(d);

    c.imprimirGaraje();

    System.out.println(c.buscarCochesPorMarca("Ferrari"));


  }


}
