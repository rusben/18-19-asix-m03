class Marca {

  private String nombre;
  private int garantia;

  Marca(String nombre, int garantia) {
    this.nombre = nombre;
    this.garantia = garantia;
  }


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getGarantia() {
		return garantia;
	}

	public void setGarantia(int garantia) {
		this.garantia = garantia;
	}
}
