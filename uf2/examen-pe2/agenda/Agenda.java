
class Agenda {

  Contacto[] contactos;

  Agenda() {
    this.contactos = new Contacto[10];
    initContactos();
  }

  Agenda(int tamano) {
    this.contactos = new Contacto[tamano];
    initContactos();
  }

  private void initContactos() {
    for (int i = 0; i < this.contactos.length; i++) {
      this.contactos[i] = new Contacto();
    }
  }

  public boolean insertaContacto(Contacto c) {

    if (!existeContacto(c)) {
      for (int i = 0; i < this.contactos.length; i++) {
        if (this.contactos[i] == null) {
          this.contactos[i] = c;
          return true;
        }
      }
    }

    return false;
  }

  public boolean existeContacto(Contacto c) {
    for (int i = 0; i < this.contactos.length; i++) {
      if (this.contactos[i].getNombre() == c.getNombre()) {
        return true;
      }
    }
    return false;
  }

  public void imprimirAgenda() {
    for (int i = 0; i < this.contactos.length; i++) {
      if (this.contactos[i] != null) {
        System.out.println(this.contactos[i]);
      }
    }
  }

  public Contacto buscarContacto(String nombre) {
    for (int i = 0; i < this.contactos.length; i++) {
      if (this.contactos[i] != null) {
        if (nombre.equals(this.contactos[i].getNombre())) {
          return this.contactos[i];
        }
      }
    }
    return null;
  }

  public boolean eliminarContacto(Contacto c) {
    for (int i = 0; i < this.contactos.length; i++) {
      if (this.contactos[i].getNombre() == c.getNombre()) {
        this.contactos[i] = null;
        return true;
      }
    }
    return false;
  }

  public boolean agendaLlena() {
    for (int i = 0; i < this.contactos.length; i++) {
      if (this.contactos[i] != null) {
        return false;
      }
    }
    return true;
  }

  public int huecosLibres() {
    int huecos = 0;
    for (int i = 0; i < this.contactos.length; i++) {
      if (this.contactos[i] == null) {
        huecos++;
      }
    }
    return huecos;
  }

}
