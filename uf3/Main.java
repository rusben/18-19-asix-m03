import java.io.*;

class TestData {
   int id;
   String value;
   String value2;
}

public class Main {

   static final String SEPARATOR = ":";
   static final String FILENAME = "myfile";

   public static void main(String[] args) throws IOException {
       File file = new File(FILENAME);

       File f = new File("hola.txt");

       // borrar datos previos
       file.delete();

       TestData testData = new TestData();

       testData.id = 11;
       testData.value = "abcd";
       testData.value2 = "6789";
       create(testData, file);

       testData.id = 22;
       testData.value = "lmno";
       testData.value2 = "4567";
       create(testData, file);

       testData.id = 33;
       testData.value = "hijk";
       testData.value2 = "9012";
       create(testData, file);

       testData.id = 44;
       testData.value = "pqrs";
       testData.value2 = "5432";
       create(testData, file);

       TestData testData33 = read(33, file);
       if(testData33 != null){
           System.out.println(testData33.id +" " + testData33.value + " "+ testData33.value2);
       }

       testData.id = 22;
       testData.value = "wxyz";
       update(testData, file);

       testData.id = 33;
       delete(testData, file);

       testData33 = read(33, file);
       if(testData33 != null){
           System.out.println(testData33.id +" " + testData33.value + " "+ testData33.value2);
       } else {
           System.out.println("id: 33 not found");
       }
   }

   static void create(TestData testData, File file) throws IOException {
       BufferedWriter outputStream = new BufferedWriter(new FileWriter(file, true));

       outputStream.write(testData.id + SEPARATOR + testData.value + SEPARATOR + testData.value2 + "\n");

       outputStream.close();
   }

   static TestData read(int id, File file) throws IOException {
       BufferedReader inputStream = new BufferedReader(new FileReader(file));

       TestData testData = null;
       String line;
       while((line = inputStream.readLine()) != null){
           String[] values = line.split(SEPARATOR);

           if(Integer.valueOf(values[0]) == id){
               testData = new TestData();
               testData.id = Integer.valueOf(values[0]);
               testData.value = values[1];
               testData.value2 = values[2];
           }
       }

       inputStream.close();

       return testData;
   }

   static void update(TestData testData, File file) throws IOException {
       File tmpFile = new File(file.getAbsolutePath() + "tmp");

       BufferedReader inputStream = new BufferedReader(new FileReader(file));
       BufferedWriter outputStream = new BufferedWriter(new FileWriter(tmpFile));

       String line;
       while((line = inputStream.readLine()) != null){
           String[] values = line.split(SEPARATOR);

           if(Integer.valueOf(values[0]) == testData.id){
               outputStream.write(testData.id + SEPARATOR + testData.value + SEPARATOR + testData.value2 + "\n");
           } else {
               outputStream.write(line + "\n");
           }
       }

       outputStream.close();
       inputStream.close();

       tmpFile.renameTo(file);
   }

   static void delete(TestData testData, File file) throws IOException {
       File tmpFile = new File(file.getAbsolutePath() + "tmp");

       BufferedReader inputStream = new BufferedReader(new FileReader(file));
       BufferedWriter outputStream = new BufferedWriter(new FileWriter(tmpFile));

       String line;
       while((line = inputStream.readLine()) != null){
           String[] values = line.split(SEPARATOR);

           if(Integer.valueOf(values[0]) != testData.id){
               outputStream.write(line + "\n");
           }
       }

       outputStream.close();
       inputStream.close();

       tmpFile.renameTo(file);
   }
}
