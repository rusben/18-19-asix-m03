import java.io.*;


class F3 {

  public static void main(String[] args)  throws IOException {
    File f = new File("F2.in");
    BufferedReader inputStream = new BufferedReader(new FileReader(f));

    Fruta[] cesta = new Fruta[100];


    String line;
    int i = 0;
    while((line = inputStream.readLine()) != null) {
          String[] values = line.split(":");

          // Creamos la Fruta
          Fruta fru = new Fruta(values[0], values[1], Integer.parseInt(values[2]));
          cesta[i] = fru;

          // Comprobación
          System.out.println(cesta[i].nombre);
          i++;
    }

    i--;
    cesta[i] = new Fruta("Watermelon", "green", 2000);
    i++;
    cesta[i] = new Fruta("Apple", "yellow", 15);


    File f3 = new File("F3.salida");
    // Preparamos el BufferedWriter
    BufferedWriter outputStream = new BufferedWriter(new FileWriter(f3, false));

    // Recorremos la cesta
    for (int j = 0; j < cesta.length; j++) {
      if (cesta[j] instanceof Fruta) {
        // Atención el carácter  :   es el separador y salto de línea al final del archivo
        outputStream.write(cesta[j].nombre+":"+cesta[j].color+":"+cesta[j].gramos+"\n");
      }
    }

    outputStream.close();

  }

}

class Fruta​ {
  String nombre;
  String color;
  int​ gramos;

  Fruta(String n, String c, int g) {
    this.nombre = n;
    this.color = c;
    this.gramos = g;
  }
}
