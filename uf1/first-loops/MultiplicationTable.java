import java.util.Scanner;

public class MultiplicationTable {
   public static void main(String[] args) {
      // This program reads a number > 0 and writes the multiplication table for that number.
      Scanner in = new Scanner(System.in);
      int x = in.nextInt();

      for(int i = 0; i <= 10; i++) {
        System.out.println(x + " * " + i + " = " + x*i);
      }

   }
}
