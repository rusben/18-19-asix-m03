import java.util.Scanner;

public class FirstNumbers {
   public static void main(String[] args) {
      // This program reads a number > 0 and writes all integers between 0 and n
      Scanner in = new Scanner(System.in);
      int x = in.nextInt();
      int i = 0;

      while (i <= x) {
        System.out.print(i + " ");
        i++; // Equivalente a i = i+1;
      }

      System.out.println();

   }
}
