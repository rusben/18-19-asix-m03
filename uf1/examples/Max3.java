import java.util.Scanner;

public class Max3 {
   public static void main(String[] args) {
      // This program reads two numbers and writes their sum
      Scanner in = new Scanner(System.in);
      int x = in.nextInt();
      int y = in.nextInt();
      int z = in.nextInt();

      if (x > y) {
      	if (x > z) {
	  System.out.println("The maximum is: "+x);
	} else {
	  System.out.println("The maximum is: "+z);
	}
      } else {
      	if (y > z) {
	  System.out.println("The maximum is: "+y);
	} else {
	  System.out.println("The maximum is: "+z);
	}
      }
   }
}
