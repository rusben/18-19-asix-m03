/*

Write a program that manages an array for a class of 10 students.
For each student we want to save the following information (DNI, name, surnames, mark). The program must allow:
- Enter student data.
- Show the list of students. (dni, name, surnames, mark).
- Update the mark of a student.
- Show the students with marks below 5.

*/

import java.util.Scanner;

public class Arrays4 {
   public static void main(String[] args) {

     Scanner in = new Scanner(System.in);

     int elements = 10;

     String[] dnis = new String[elements];
     String[] names = new String[elements];
     String[] surnames = new String[elements];
     int[] marks = new int[elements];


      for (int i=0; i < elements; i++) {
      //  System.out.println("Introduzca el DNI del estudiante:");
        dnis[i] = in.next();
      //  System.out.println("Introduzca el nombre del estudiante:");
        names[i] = in.next();
      //  System.out.println("Introduzca el apellido del estudiante:");
        surnames[i] = in.next();
    //    System.out.println("Introduzca la nota del estudiante:");
        marks[i] = in.nextInt();
      }

      for (int i=0; i < elements; i++) {
        System.out.println("Estudiante "+i);
        System.out.println("---------------");
        System.out.println("DNI: "+dnis[i]);
        System.out.println("Name: "+names[i]);
        System.out.println("Surname: "+surnames[i]);
        System.out.println("Mark: "+marks[i]);
        System.out.println("---------------");

      }


  }

  public static void menu() {
    System.out.println("Muestra la lista de estudiantes.");
    System.out.println("Actualizar la nota de un estudiante.");
    System.out.println("Muestra los estudiantes con nota menor que 5.");
  }


}
