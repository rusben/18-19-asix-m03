/*
Write a program that given an array which distributes 10 brands of cars show us, for each brand, what is it percentage of sold cars.

brands
audi, toyota, mercedes, ford, fiat, porsche, jaguar, bmw, volkswagen, seat

cars
11, 43, 67, 43, 34, 122, 66, 42, 33, 11

*/
import java.util.Random;

public class Arrays2 {
   public static void main(String[] args) {

     String brands[] = {"audi", "toyota", "mercedes", "ford", "fiat", "porsche", "jaguar", "bmw", "volkswagen", "seat"};
     int cars[] = {11, 43, 67, 43, 34, 122, 66, 42, 33, 11};

     int total = 0;
     for (int i=0; i < cars.length; i++) {
       total = total + cars[i];
     }

     for (int i=0; i < brands.length; i++) {
        System.out.println("The brand "+brands[i]+" "+(float)(cars[i]*100)/total+"% of "+total+" cars.");
     }


  }
}
