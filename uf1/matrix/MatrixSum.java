/*
Read two matrix and sum both
*/

import java.util.Scanner;

public class MatrixSum {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int x = in.nextInt();
      int y = in.nextInt();
      int[][] m = new int[x][y];
      int[][] w = new int[x][y];
      int[][] z = new int[x][y];

      // Read the m matrix
      for (int i=0; i<m.length; i++) {
        for (int j=0; j<m[0].length; j++) {
          m[i][j] = in.nextInt();
        }
      }

      // Read the w matrix
      for (int i=0; i<w.length; i++) {
        for (int j=0; j<w[0].length; j++) {
          w[i][j] = in.nextInt();
        }
      }

      // Sum m+w matrix
      for (int i=0; i<z.length; i++) {
        for (int j=0; j<z[0].length; j++) {
          z[i][j] = m[i][j] + w[i][j];
        }
      }

      // Print the matrix
      for (int i=0; i<z.length; i++) {
        for (int j=0; j<z[0].length; j++) {
            System.out.print(z[i][j]+" ");
        }
      }

   }
}
