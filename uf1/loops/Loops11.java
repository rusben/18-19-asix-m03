import java.util.Scanner;

 public class Loops11 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      Boolean allEquals = true;
      int x, y;

      x = in.nextInt();

      for (int i = 0; i < 9; i++) {

          y = in.nextInt();
          if (x != y) { allEquals = false; }

      }

      if (allEquals) System.out.println("YES");
      else System.out.println("NO");

   }
}
