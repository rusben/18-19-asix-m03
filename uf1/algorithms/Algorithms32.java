import java.util.Scanner;

public class Algorithms31 {
   public static void main(String[] args) {

     Scanner in = new Scanner(System.in);
     int x, y, grupos;
     int hayIguales = 0;

     grupos = 0;

      x = in.nextInt();
      if (x == -1) {
        System.out.println(grupos);
        System.exit(0);
      }

      y = 0;

      while (y != -1) {

        y = in.nextInt();

        if (x == y) { // Los números son iguales
          hayIguales++;
        } else { // He encontrado un cambio
            if (hayIguales > 0) {
              grupos++;
              hayIguales = 0;
            }
        }

        x = y;

      }

      System.out.println(grupos);

    }
}
