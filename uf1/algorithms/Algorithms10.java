/*
Write a program to fill a vector of 10 integers. Then the user will be asked for an integer. The program will write yes whether the number is found in the array, no otherwise.*/

import java.util.Scanner;

public class Algorithms10 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int i, x;
      int[] a = new int[10];

      for (i=0; i<10; i++) {
        x = in.nextInt();
        // Asigno a la posición i del vector el elemento
        a[i] = x;
      }

      System.out.println("Insert the number to find: ");
      // Preguntamos otro número para buscarlo
      x = in.nextInt();

      for (i=0; i<10; i++) {
        if (a[i] == x) {
          System.out.println("yes");
          System.exit(0);
        }
      }

      System.out.println("no");
   }
}
