/*
Write a program to fill a vector of 10 random integers. These integers must be between 0 and 20.
*/

import java.util.Random;

public class Algorithms11 {
   public static void main(String[] args) {

      int i, x;
      int[] a = new int[10];

      // Get a random Number
      // https://study.com/academy/lesson/java-generate-random-number-between-1-100.html
      int max = 20;
      int min = 0;

      // create instance of Random class
      Random randomNum = new Random();

      for (i=0; i<10; i++) {
        a[i] = min + randomNum.nextInt(max);
      }

      // Print the array
      for (i=0; i<10; i++) {
        System.out.println(a[i]);
      }

    }
}
