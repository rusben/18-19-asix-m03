/*
Write a program that will fill a vector of 10 integers with random between 1 and 49, without repetitions.
*/

import java.util.Random;

public class Algorithms15 {
   public static void main(String[] args) {

      int i, x;
      int counter = 0;
      int[] elements = new int[10];
      boolean found;

      // Get a random Number
      // https://study.com/academy/lesson/java-generate-random-number-between-1-100.html
      int max = 49;
      int min = 1;

      // create instance of Random class
      Random randomNum = new Random();

      while (counter < 10) {

        x = min + randomNum.nextInt(max);

        found = false;
        for (i = 0; i < elements.length ; i++ ) {
          if (elements[i] == x) {
            found = true;
            System.out.println("Iteration "+i+". Element "+x+" not added.");
            break;
          }
        }

        // Check if the element was found
        if (!found) {
          elements[counter] = x;
          counter++;
        }

      }

      // Print the array
      for (i=0; i<10; i++) {
        System.out.println(elements[i]);
      }

    }
}
